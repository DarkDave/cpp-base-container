FROM alpine:3.16
LABEL com.example.version="0.0.1"
LABEL org.opencontainers.image.authors="bauke.dawid@gmail.com"

# Timezone config.
ENV DEBIAN_FRONTEND=noninteractive 
ENV TZ=Europe/Warsaw
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apk update
RUN apk add build-base cmake git ninja bash

RUN git clone https://github.com/google/googletest

RUN mkdir -p /build/googletest
RUN cmake -GNinja -S googletest/ -B /build/googletest
RUN cmake --build /build/googletest --target install
