#pragma ONCE


class Library {
public:
    void setA(int a);
    int getA() const;

private:
    int a = 0;
};
