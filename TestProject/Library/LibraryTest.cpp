#include <gtest/gtest.h>

#include "Library.hpp"

TEST(LibraryTest, SettingAndGetting) {
    Library lib;

    EXPECT_EQ(lib.getA(), 0);

    lib.setA(1);
    EXPECT_EQ(lib.getA(), 1);
}
