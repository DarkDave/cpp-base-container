SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $SCRIPT_DIR
mkdir -p build

docker run -it --mount type=bind,source=$SCRIPT_DIR/TestProject,target=/TestProject \
    --mount type=bind,source=$SCRIPT_DIR/build,target=/build/TestProject \
    --rm cpp bash -c "cmake -GNinja -S/TestProject -B /build/TestProject -DCMAKE_C++_COMPILER=$CXX\
                            && cmake --build /build/TestProject \
                            && echo 'Running TestProject' \
                            && /build/TestProject/TestProject \
                            && ctest --test-dir /build/TestProject" 
