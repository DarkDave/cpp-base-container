# ( Sample project for building and testing C++ code with Docker and GoogleTest.

Run image_build.sh to create Docker image, then you can build and test the TestProject with a subtarget (Library) by running run.sh.
It will run the main.cpp in TestProject and tests in TestProject and TestProject/Library.

You can use enter.sh to just enter bash in docker for inspection.

:warning: **This is a very early version, for now it just works on sample projects, any changes require editing existing projects.**
